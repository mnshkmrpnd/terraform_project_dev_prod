# Variable for Create Instance Module
variable "public_key_path" {
  description = "Public key path"
  default = "/home/manish.pandey/terraform/Terraform_project/Terraform_Project_Structure/Development/levelup_key.pub"
}

variable "VPC_ID" {
    type = string
    default = ""
}

variable "ENVIRONMENT" {
    type    = string
    default = ""
}

variable "AWS_REGION" {
default = "ap-south-1"
}

variable "AMIS" {
    type = map
    default = {
        #ap-south-1 = "ami-01a4f99c4ac11b03c"
        #ap-south-1 = "ami-06984ea821ac0a879"
        ap-south-1 = "ami-0f9d9a251c1a44858"
        
    }
}

variable "INSTANCE_TYPE" {
  default = "t2.micro"
}

variable "PUBLIC_SUBNETS" {
  type = list
}