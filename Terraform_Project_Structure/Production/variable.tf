variable "AWS_REGION" {
    type    = string
    default = "ap-south-1"
}

variable "Env" {
    type    = string
    default = "production"
}